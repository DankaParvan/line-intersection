## Line Intersection

Please fill the method `getIntersection(Line, int)` in class [`Line`](src/main/java/com/epam/rd/autotasks/intersection/Line.java). It  must return a [`Point`](src/main/java/com/epam/rd/autotasks/intersection/Point.java) of intersection of two lines. You may check your result in class [`Main`](com/epam/rd/autotasks/intersection/Main.java).</br>

`Note:` if lines coincide or do not intersect, the method must return null.
